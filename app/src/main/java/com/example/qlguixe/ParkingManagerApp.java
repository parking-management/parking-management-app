package com.example.qlguixe;

import android.app.Application;
import android.os.Build;

import androidx.annotation.RequiresApi;

import com.example.qlguixe.utils.SharedPreferenceUtil;

public class ParkingManagerApp extends Application {
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onCreate() {
        super.onCreate();
        SharedPreferenceUtil.init(this);
    }
}
