package com.example.qlguixe.api;


import com.example.qlguixe.models.Account;
import com.example.qlguixe.models.Statistical;
import com.example.qlguixe.models.Transport;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {
    String DOMAIN = "https://fluffy-tick-spacesuit.cyclic.app/api/";
    Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").setLenient().create();

    ApiService apiService = new Retrofit.Builder()
            .baseUrl(DOMAIN)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
            .create(ApiService.class);


    @POST("user/login")
    Call<Account> login(@Body Account request);

    @POST("user")
    Call<Account> register(@Body Account account);

    @GET("transport")
    Call<List<Transport>> getTransportOfUser(@Query("iduser") String idUser);

    @POST("transport")
    Call<Transport> registerTransport(@Body Transport transport);

    @GET("statistical")
    Call<List<Statistical>> getAllIn(@Query("isout") String isout);

    @POST("statistical")
    Call<Statistical> transportGetIn(@Query("trans_license") String trans_license);

    @PATCH("statistical")
    Call<Statistical> transportGetOut(@Query("qr") String qr);

    @GET("user")
    Call<List<Account>> getAllAccount();

    @PATCH("user/{idUser}")
    Call<Account> updateUser(@Path("idUser") String idUser, @Body Account account);

    @DELETE("user/{idUser}")
    Call<Account> deleteAccount(@Path("idUser") String idUser);
}
