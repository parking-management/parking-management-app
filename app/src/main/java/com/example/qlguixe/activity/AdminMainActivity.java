package com.example.qlguixe.activity;

import static com.example.qlguixe.fragment.ParkingFragment.listStatistical;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.example.qlguixe.R;
import com.example.qlguixe.api.ApiService;
import com.example.qlguixe.fragment.AccountFragment;
import com.example.qlguixe.fragment.ParkingFragment;
import com.example.qlguixe.fragment.StatisticalFragment;
import com.example.qlguixe.models.Statistical;
import com.example.qlguixe.utils.FunctionUtils;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.text.FirebaseVisionText;
import com.google.firebase.ml.vision.text.FirebaseVisionTextDetector;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.ismaeldivita.chipnavigation.ChipNavigationBar;

import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AdminMainActivity extends AppCompatActivity {

    public static boolean TRANSPORT_GET_IN = false;
    public static boolean TRANSPORT_GET_OUT = false;

    private final String TAG = "AdminMainActivity";
    private final Context context = AdminMainActivity.this;

    private Bitmap imageBitmap;
    private ChipNavigationBar chipNavigationBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Khởi tạo view
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_main);

        // Ẩn action bar phía trên
        Objects.requireNonNull(getSupportActionBar()).hide();

        // Tìm kiếm và ánh xạ các thành phần trong view
        initView();

        // Khởi tạo các listener lắng nghe các hành động
        initListener();
    }

    private void initView() {
        // Tìm kiếm và ánh xạ menu của admin
        chipNavigationBar = findViewById(R.id.bottomNav);

        // Khởi tạo trang mặc định ban đầu là parkingFragment
        chipNavigationBar.setItemSelected(R.id.itemParking, true);
        getSupportFragmentManager().beginTransaction().replace(R.id.container, new ParkingFragment()).commit();
    }

    @SuppressLint("NonConstantResourceId")
    private void initListener() {
        // Khởi tạo sự kiện khi bấm chọn Item dưới Menu Admin
        chipNavigationBar.setOnItemSelectedListener(i -> {
            Fragment fragment = null;
            switch (i) {
                case R.id.itemParking:
                    fragment = new ParkingFragment();
                    break;
                case R.id.itemStatistical:
                    fragment = new StatisticalFragment();
                    break;
                case R.id.itemAccount:
                    fragment = new AccountFragment();
                    break;
                default:
                    break;
            }

            if (fragment != null) {
                getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(context, "Bạn chưa cấp quyền cho phép sử dụng máy ảnh !", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Gửi xe vào bãi
        if (TRANSPORT_GET_IN) {
            transportGetIn(data);
        }

        // Lấy xe ra khỏi bãi
        if (TRANSPORT_GET_OUT) {
            // Gán lại giá trị cờ
            TRANSPORT_GET_OUT = false;

            // Khởi tạo IntentResult bằng cách parse Activity Result
            IntentResult intentResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            if (intentResult == null || intentResult.getContents() == null) {
                Toast.makeText(context, "Lỗi không xác định !", Toast.LENGTH_SHORT).show();
            } else {
                transportGetOut(intentResult);
            }
        }
    }

    private void transportGetIn(Intent data) {
        // Lấy ra ảnh biển số xe đã chụp được
        Bundle extras = Objects.requireNonNull(data).getExtras();
        imageBitmap = (Bitmap) extras.get("data");

        // Khởi tạo Dialog xác nhận biển số xe vào bãi
        Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_confirm_transport_license);

        // Tìm kiếm và ánh xạ các thành phần trong Dialog
        Button btnConfirm = dialog.findViewById(R.id.btnConfirm);
        TextView txtTransportLicense = dialog.findViewById(R.id.txtTransportLicense);
        ImageView imgTransportLicense = dialog.findViewById(R.id.imgTransportLicense);

        // Set ảnh biển số vừa chụp được vào Dialog
        imgTransportLicense.setImageBitmap(imageBitmap);

        // Set giá trị biển số xe đọc được vào Dialog
        getTransportLicense(txtTransportLicense, btnConfirm);

        // Khởi tạo sự kiện khi bấm nút Xác nhận
        btnConfirm.setOnClickListener(view -> {
            // Lấy ra thời gian gọi API
            long timeCallAPI = System.currentTimeMillis();

            // Lấy ra biển số xe
            String[] strTransportLicense = String.valueOf(txtTransportLicense.getText()).split(":");
            String transportLicense = strTransportLicense[1].trim();

            Log.i(TAG, "Transport " + transportLicense + " get in " + timeCallAPI);

            // Gọi API gửi xe vào bãi
            ApiService.apiService.transportGetIn(transportLicense).enqueue(new Callback<Statistical>() {
                @Override
                public void onResponse(Call<Statistical> call, Response<Statistical> response) {
                    try {
                        if (response.isSuccessful()) {
                            Log.i(TAG, "Transport " + transportLicense + " get in " + timeCallAPI + " success");

                            // Lấy ra kết quả và cập nhật danh sách xe đang gửi trong bãi
                            Statistical statistical = response.body();
                            listStatistical.add(statistical);
                            ParkingFragment.transportInParkingAdapter.setListStatistical(listStatistical);

                            // Hiển thị lại số xe đang có trong bãi
                            ParkingFragment.binding.txtNumberTransportInParking.setText(
                                    FunctionUtils.designTextWhiteAndYellow(
                                            "Số xe đang có trong bãi:",
                                            listStatistical.size() + "/1000 xe"));

                            // Hiển thị thông báo
                            Toast.makeText(context, "Cho xe vào bãi thành công !", Toast.LENGTH_SHORT).show();
                        } else {
                            // Lấy ra response lỗi
                            String error = Objects.requireNonNull(response.errorBody()).string();

                            // Convert từ String sang JsonObject
                            JsonObject errorResponse = new Gson().fromJson(error, JsonObject.class);

                            // Lấy ra message thông báo lỗi và hiển thị cho người dùng
                            String errorMessage = errorResponse.get("message").getAsString();
                            Log.e(TAG, "Transport " + transportLicense + " get in " + timeCallAPI + " failed: " + errorMessage);
                            Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "Transport " + transportLicense + " get in " + timeCallAPI + " error: " + e.getMessage());
                        Log.getStackTraceString(e);
                        Toast.makeText(context, "Lỗi không xác định !", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<Statistical> call, Throwable t) {
                    Log.e(TAG, "Transport " + transportLicense + " get in " + timeCallAPI + " error: " + t.getMessage());
                    Log.getStackTraceString(t);
                    Toast.makeText(context, "Lỗi không xác định !", Toast.LENGTH_SHORT).show();
                }
            });

            // Ẩn Dialog xác nhận biển số xe vào bãi
            dialog.dismiss();
        });

        // Gán lại giá trị cờ và hiển thị Dialog
        TRANSPORT_GET_IN = false;
        dialog.show();
    }

    private void getTransportLicense(TextView txtTransportLicense, Button btnConfirm) {
        // Khởi tạo FirebaseVisionImage từ ảnh biển số xe vừa chụp được
        FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(imageBitmap);

        // Khởi tạo FirebaseVisionTextDetector dùng để nhận diện và lấy ra chuỗi trong ảnh
        FirebaseVisionTextDetector detector = FirebaseVision.getInstance().getVisionTextDetector();

        // Lấy ra thời gian xử lý
        long time = System.currentTimeMillis();

        // Khởi tạo sự kiện khi nhận diện và lấy ra chuỗi trong ảnh
        detector.detectInImage(image).addOnSuccessListener(text -> {
            // Lấy ra các block chuỗi
            List<FirebaseVisionText.Block> blocks = text.getBlocks();

            // Kiểm tra số lượng các block và báo lỗi nếu có
            if (blocks.size() == 0) {
                Toast.makeText(context, "Không đọc được biển số xe !", Toast.LENGTH_SHORT).show();
            }

            // Tạo biến lưu biển số xe
            String transportLicense = "";

            // Lấy ra dữ liệu của từng block và cộng lại để ra biển số xe cuối cùng
            for (FirebaseVisionText.Block block : text.getBlocks()) {
                transportLicense = block.getText();
                Log.i(TAG, "Part of transport license has read: " + transportLicense);

                for (int i = 0; i < transportLicense.length(); i++) {
                    // Kiểm tra ký tự trong chuỗi đọc được có phải số hoặc chữ in hoa (dựa vào bảng ASCII)
                    boolean isNotNumber = (int) transportLicense.charAt(i) < 48 || (int) transportLicense.charAt(i) > 58;
                    boolean isNotLetter = (int) transportLicense.charAt(i) < 65 || (int) transportLicense.charAt(i) > 90;

                    // Nếu không phải số và cũng không phải chữ in hoa thì cắt bỏ ký tự đấy đi
                    if (isNotNumber && isNotLetter) {
                        transportLicense = transportLicense.substring(0, i) + transportLicense.substring(i + 1);
                    }
                }
            }

            // Kiểm tra biển số xe đọc được cuối cùng, thực thi gửi xe vào bãi và thông báo kết quả cho người dùng
            if (StringUtils.isNotBlank(transportLicense)) {
                Log.i(TAG, "Read transport license " + time + " success: " + transportLicense);

                // Set giá trị ô hiển thị biển số xe
                txtTransportLicense.setText(
                        FunctionUtils.designTextBlackAndRed(
                                "Biển số xe:",
                                transportLicense));

                // Bật nút Xác nhận cho người dùng
                btnConfirm.setTextColor(Color.WHITE);
                btnConfirm.setBackgroundColor(Color.BLUE);
                btnConfirm.setEnabled(true);
            } else {
                Log.i(TAG, "Read transport license " + time + " failed");
                Toast.makeText(context, "Không đọc được biển số xe !", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(e -> {
            Log.i(TAG, "Read transport license " + time + " failed: " + e.getMessage());
            Toast.makeText(context, "Không đọc được biển số xe !", Toast.LENGTH_SHORT).show();
        });
    }

    private void transportGetOut(IntentResult intentResult) {
        // Lấy ra thời gian xử lý
        long time = System.currentTimeMillis();

        // Lấy ra mã QR
        String qr = intentResult.getContents();

        Log.e(TAG, "Transport get out " + time + " with QR " + qr);

        // Kiểm tra độ dài mã QR trước khi gọi API cho xe ra khỏi bãi
        if (qr.length() < 10) {
            Log.e(TAG, "Transport get out " + time + " failed: QR length is invalid");
            Toast.makeText(context, "Mã QR không hợp lệ !", Toast.LENGTH_SHORT).show();
        } else {
            // Gọi API cho xe ra khỏi bãi
            ApiService.apiService
                    .transportGetOut(intentResult.getContents())
                    .enqueue(new Callback<Statistical>() {
                        @Override
                        @RequiresApi(api = Build.VERSION_CODES.N)
                        public void onResponse(Call<Statistical> call, Response<Statistical> response) {
                            try {
                                if (response.isSuccessful() && response.body() != null) {
                                    Statistical carOut = response.body();

                                    // Hiển thị Dialog thông tin xe ra khỏi bãi
                                    showDialogInfoTransportGetOut(carOut);

                                    // Hiển thị lại danh sách xe trong bãi
                                    List<Statistical> tmpListStatistical = listStatistical.stream()
                                            .filter(i -> !i.get_id().equals(carOut.get_id()))
                                            .collect(Collectors.toList());
                                    ParkingFragment.transportInParkingAdapter.setListStatistical(tmpListStatistical);

                                    // Hiển thị lại số xe đang có trong bãi
                                    ParkingFragment.binding.txtNumberTransportInParking.setText(
                                            FunctionUtils.designTextWhiteAndYellow(
                                                    "Số xe đang có trong bãi:",
                                                    tmpListStatistical.size() + "/1000 xe"));

                                    // Hiển thị thông báo cho người dùng
                                    Toast.makeText(context, "Cho xe ra khỏi bãi thành công !", Toast.LENGTH_SHORT).show();
                                } else {
                                    // Lấy ra response lỗi
                                    String error = Objects.requireNonNull(response.errorBody()).string();

                                    // Convert từ String sang JsonObject
                                    JsonObject errorResponse = new Gson().fromJson(error, JsonObject.class);

                                    // Lấy ra message thông báo lỗi và hiển thị cho người dùng
                                    String errorMessage = errorResponse.get("message").getAsString();
                                    Log.e(TAG, "Transport get out " + time + " failed: " + errorMessage);
                                    Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
                                }
                            } catch (Exception e) {
                                Log.e(TAG, "Transport get out " + time + " error: " + e.getMessage());
                                Log.getStackTraceString(e);
                                Toast.makeText(context, "Lỗi không xác định !", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<Statistical> call, Throwable t) {
                            Log.e(TAG, "Transport get out " + time + " error: " + t.getMessage());
                            Log.getStackTraceString(t);
                            Toast.makeText(context, "Lỗi không xác định !", Toast.LENGTH_SHORT).show();
                        }
                    });
        }
    }

    private void showDialogInfoTransportGetOut(Statistical carOut) {
        // Khởi tạo Form xuất xe
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.item_transport_in_parking);
        dialog.setCancelable(true);

        // Khởi tạo background đằng sau
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams windowAttributes = window.getAttributes();
        windowAttributes.gravity = Gravity.CENTER_VERTICAL;
        window.setAttributes(windowAttributes);

        // Tìm kiếm và ánh xạ các thành phần có trên Dialog
        TextView titleTransportGetOut = dialog.findViewById(R.id.titleTransportGetOut);
        TextView txtTypeTransport = dialog.findViewById(R.id.txtTypeTransport);
        TextView txtNameTransport = dialog.findViewById(R.id.txtNameTransport);
        TextView txtOwnTransport = dialog.findViewById(R.id.txtOwnTransport);
        TextView txtLicenseTransport = dialog.findViewById(R.id.txtLicenseTransport);
        TextView txtTimeGetIn = dialog.findViewById(R.id.txtTimeGetIn);
        TextView txtTimeGetOut = dialog.findViewById(R.id.txtTimeGetOut);

        // Gán thông tin xe đã xuất vào các thành phần
        titleTransportGetOut.setVisibility(View.VISIBLE);
        txtTypeTransport.setText(
                FunctionUtils.designTextBlackAndBlue(
                        "Loại xe:",
                        carOut.getTransport().getTrans_type()));
        txtNameTransport.setText(
                FunctionUtils.designTextBlackAndBlue(
                        "Tên xe:",
                        carOut.getTransport().getTrans_name()));
        txtOwnTransport.setText(
                FunctionUtils.designTextBlackAndBlue(
                        "Chủ xe:",
                        carOut.getTransport().getOwn().getUsername()));
        txtLicenseTransport.setText(
                FunctionUtils.designTextBlackAndBlue(
                        "Biển số:",
                        carOut.getTransport().getTrans_license()));
        txtTimeGetIn.setText(
                FunctionUtils.designTextBlackAndBlue(
                        "Thời gian xe vào:",
                        carOut.getTimeCome()));
        txtTimeGetOut.setText(
                FunctionUtils.designTextBlackAndBlue(
                        "Thời gian xe ra",
                        carOut.getTimeOut()));
        txtTimeGetOut.setVisibility(View.VISIBLE);

        // Hiển thị Dialog thông tin xe ra
        dialog.show();
    }
}