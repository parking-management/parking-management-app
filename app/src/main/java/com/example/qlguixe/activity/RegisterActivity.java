package com.example.qlguixe.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.qlguixe.R;
import com.example.qlguixe.api.ApiService;
import com.example.qlguixe.models.Account;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.apache.commons.lang3.StringUtils;

import java.util.Objects;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    private final String TAG = "RegisterActivity";
    private final Context context = RegisterActivity.this;

    private TextView txtError;
    private Button btnRegister;
    private EditText edtUsername, edtPassword, edtConfirmPassword, edtName, edtPhoneNumber, edtAddress, edtDateOfBirth, edtEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Khởi tạo view
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        // Ẩn action bar phía trên
        Objects.requireNonNull(getSupportActionBar()).hide();

        // Tìm kiếm và ánh xạ các thành phần trong view
        initView();

        // Khởi tạo các listener lắng nghe các hành động
        initListener();
    }

    private void initView() {
        // Tìm kiếm và ánh xạ nút Đăng ký
        btnRegister = findViewById(R.id.btnRegister);

        // Tìm kiếm và ánh xạ textView thông báo lỗi
        txtError = findViewById(R.id.txtError);

        // Ẩn thông báo lỗi
        txtError.setVisibility(View.GONE);

        // Tìm kiếm và ánh xạ các editText
        edtUsername = findViewById(R.id.edtUsername);
        edtPassword = findViewById(R.id.edtPassword);
        edtConfirmPassword = findViewById(R.id.edtConfirmPassword);
        edtName = findViewById(R.id.edtName);
        edtPhoneNumber = findViewById(R.id.edtPhoneNumber);
        edtAddress = findViewById(R.id.edtAddress);
        edtDateOfBirth = findViewById(R.id.edtDateOfBirth);
        edtEmail = findViewById(R.id.edtEmail);
    }

    private void initListener() {
        // Khởi tạo sự kiện khi bấm Đăng ký
        btnRegister.setOnClickListener(view -> {
            // Lấy ra thông tin đăng ký tài khoản
            String username = edtUsername.getText().toString().trim();
            String password = edtPassword.getText().toString().trim();
            String confirmPassword = edtConfirmPassword.getText().toString().trim();
            String name = edtName.getText().toString().trim();
            String phoneNumber = edtPhoneNumber.getText().toString().trim();
            String address = edtAddress.getText().toString().trim();
            String dob = edtDateOfBirth.getText().toString().trim();
            String email = edtEmail.getText().toString().trim();

            // Kiểm tra request đăng ký tài khoản trước khi gọi API
            String error = validateRegisterRequest(
                    username, password, confirmPassword, name, phoneNumber, address, dob, email);
            if (StringUtils.isNotBlank(error)) {
                txtError.setText(error);
                txtError.setVisibility(View.VISIBLE);
            } else {
                // Lấy ra thời gian xử lý sự kiện
                long time = System.currentTimeMillis();

                // Khởi tạo request đăng ký tài khoản
                Account request = new Account(username, password, name, phoneNumber, address, dob, email);
                Log.i(TAG, "Register " + time + " request: " + request);

                // Gọi API đăng ký tài khoản
                ApiService.apiService.register(request).enqueue(new Callback<Account>() {
                    @Override
                    public void onResponse(Call<Account> call, Response<Account> response) {
                        try {
                            if (response.isSuccessful()) {
                                Log.i(TAG, "Register " + time + " success ");

                                // Ẩn thông báo lỗi nếu có
                                txtError.setVisibility(View.GONE);

                                // Quay về trang đăng nhập và hiển thị thông báo cho người dùng
                                Intent intent = new Intent(context, LoginActivity.class);
                                intent.putExtra("notification", "Đăng ký tài khoản thành công !");
                                startActivity(intent);
                                finish();
                            } else {
                                // Lấy ra response lỗi
                                String error = Objects.requireNonNull(response.errorBody()).string();

                                // Convert từ String sang JsonObject
                                JsonObject errorResponse = new Gson().fromJson(error, JsonObject.class);

                                // Lấy ra message thông báo lỗi và hiển thị cho người dùng
                                String errorMessage = errorResponse.get("message").getAsString();
                                Log.e(TAG, "Register " + time + " failed: " + errorMessage);
                                txtError.setText(errorMessage);
                                txtError.setVisibility(View.VISIBLE);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Register " + time + " error: " + e.getMessage());
                            Log.getStackTraceString(e);
                            txtError.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onFailure(Call<Account> call, Throwable t) {
                        Log.e(TAG, "Register " + time + " error: " + t.getMessage());
                        Log.getStackTraceString(t);
                        txtError.setVisibility(View.VISIBLE);
                    }
                });
            }
        });
    }

    public static String validateRegisterRequest(
            String username, String password, String confirmPassword, String name, String phoneNumber,
            String address, String dob, String email) {
        // Khởi tạo các pattern để validate
        Pattern patternPhone = Pattern.compile("^[0-9]*$");
        Pattern patternEmail = Pattern.compile("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$");
        Pattern patternPassword = Pattern.compile("^(?=.*[A-Za-z])(?=.*\\d)(?=.*[@$!%*#?&])[A-Za-z\\d@$!%*#?&]{6,}$");

        // Kiểm tra người dùng đã nhập đầy đủ thông tin chưa ?
        if (StringUtils.isBlank(username)
                || StringUtils.isBlank(password)
                || StringUtils.isBlank(confirmPassword)
                || StringUtils.isBlank(name)
                || StringUtils.isBlank(phoneNumber)
                || StringUtils.isBlank(address)
                || StringUtils.isBlank(dob)
                || StringUtils.isBlank(email)) {
            return "Xin hãy điền đủ thông tin yêu cầu !";
        }

        // Kiểm tra độ dài tên đăng nhập
        if (username.length() < 3) {
            return "Tên đăng nhập phải tối thiểu trên 3 kí tự !";
        }

        // Kiểm tra mật khẩu có đúng với quy định
        // Tối thiểu 6 ký tự trong đó có ít nhất 1 chữ, 1 số và 1 ký tự đặc biệt
        if (!patternPassword.matcher(password).find()) {
            return "Mật khẩu phải tối thiểu 6 ký tự và có ít nhất 1 chữ, 1 số và 1 ký tự đặc biệt !";
        }

        // Kiểm tra mật khẩu nhập lại
        if (!password.equals(confirmPassword)) {
            return "Mật khẩu nhập lại không đúng !";
        }

        // Kiểm tra số điện thoại có hợp lệ ?
        if (phoneNumber.length() < 10 || !patternPhone.matcher(phoneNumber).find()) {
            return "Số điện thoại không hợp lệ !";
        }

        // Kiểm tra email có hợp lệ ?
        if (!patternEmail.matcher(email).find()) {
            return "Email không hợp lệ !";
        }

        return "";
    }

    @Override
    public void onBackPressed() {
        // Quay về trang đăng nhập và kết thúc
        startActivity(new Intent(context, LoginActivity.class));
        finish();
    }
}