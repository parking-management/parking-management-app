package com.example.qlguixe.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.qlguixe.R;
import com.example.qlguixe.api.ApiService;
import com.example.qlguixe.models.Account;
import com.example.qlguixe.utils.AppConstants;
import com.example.qlguixe.utils.SharedPreferenceUtil;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private final String TAG = "LoginActivity";
    private final Context context = LoginActivity.this;

    private Button btnLogin;
    private CheckBox cbRemember;
    private EditText edtUsername, edtPassword;
    private TextView txtRegister, txtError;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Khởi tạo view
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Ẩn action bar phía trên
        Objects.requireNonNull(getSupportActionBar()).hide();

        // Tìm kiếm và ánh xạ các thành phần trong view
        initView();

        // Khởi tạo các listener lắng nghe các hành động
        initListener();

        // Hiển thị thông báo nếu có
        String notification = getIntent().getStringExtra("notification");
        if (StringUtils.isNotBlank(notification)) {
            Toast.makeText(context, notification, Toast.LENGTH_LONG).show();
        }
    }

    private void initView() {
        // Tìm kiếm và ánh xạ nút Đăng nhập
        btnLogin = findViewById(R.id.btnLogin);

        // Tìm kiếm và ánh xạ ô nhập Username và Password
        edtUsername = findViewById(R.id.edtUsername);
        edtPassword = findViewById(R.id.edtPassword);

        // Tìm kiếm và ánh xạ check box Lưu đăng nhập
        cbRemember = findViewById(R.id.cbRemember);

        // Tìm kiếm và ánh xạ các textView
        txtError = findViewById(R.id.txtError);
        txtRegister = findViewById(R.id.txtRegister);

        // Ẩn thông báo lỗi
        txtError.setVisibility(View.GONE);

        // Kiểm tra người dùng có chọn lưu thông tin đăng nhập không ?
        // Nếu có thì auto fill tên đăng nhập và mật khẩu đã lưu
        // Nếu không thì hiển thị mặc định
        try {
            if (SharedPreferenceUtil.getBoolean(AppConstants.REMEMBER)) {
                edtUsername.setText(SharedPreferenceUtil.getString(AppConstants.USERNAME));
                edtPassword.setText(SharedPreferenceUtil.getString(AppConstants.PASSWORD));
                cbRemember.setChecked(true);
            } else {
                edtUsername.setText(null);
                edtPassword.setText(null);
                cbRemember.setChecked(false);
            }
        } catch (Exception ex) {
            Log.i(TAG, "Autofill username and password error " + ex.getMessage());
            Log.getStackTraceString(ex);
        }
    }

    private void initListener() {
        // Khởi tạo sự kiện bấm Đăng nhập
        btnLogin.setOnClickListener(v -> {
            // Lấy ra thời gian xử lý sự kiện
            long time = System.currentTimeMillis();

            // Lấy ra Username và Password đăng nhập
            String username = edtUsername.getText().toString().trim();
            String password = edtPassword.getText().toString().trim();

            // Kiểm tra username và password trước khi gọi API
            if (StringUtils.isBlank(username) || StringUtils.isBlank(password)) {
                txtError.setText("Username và password không được để trống !");
                txtError.setVisibility(View.VISIBLE);
            } else {
                // Khởi tạo request đăng nhập
                Account request = new Account(username, password);
                Log.i(TAG, "Login " + time + " request: " + request);

                // Gọi API đăng nhập
                ApiService.apiService.login(request).enqueue(new Callback<Account>() {
                    @Override
                    public void onResponse(Call<Account> call, Response<Account> response) {
                        try {
                            if (response.isSuccessful()) {
                                // Ẩn thông báo lỗi nếu có
                                txtError.setVisibility(View.GONE);

                                // Lấy ra kết quả tài khoản trả về
                                Account currentAccount = response.body();

                                // Xử lý kết quả trả về
                                if (currentAccount != null) {
                                    Log.i(TAG, "Login " + time + " success " + currentAccount);

                                    // Lưu id tài khoản đang đăng nhập
                                    SharedPreferenceUtil.setString(AppConstants.ACCOUNT_ID, currentAccount.get_id());

                                    // Lưu tên tài khoản đang đăng nhập
                                    SharedPreferenceUtil.setString(AppConstants.USERNAME, currentAccount.getName());

                                    // Lưu mật khẩu tài khoản đang đăng nhập
                                    SharedPreferenceUtil.setString(AppConstants.PASSWORD, password);

                                    // Lưu số dư tài khoản đang đăng nhập
                                    SharedPreferenceUtil.setLong(AppConstants.SURPLUS, currentAccount.getMoney());

                                    // Lưu lựa chọn của người dùng đối với việc lưu thông tin tài khoản
                                    SharedPreferenceUtil.setBoolean(AppConstants.REMEMBER, cbRemember.isChecked());


                                    // Kiểm tra và chuyển tiếp đến trang chủ theo quyền của tài khoản
                                    if (currentAccount.getRole().equals(AppConstants.ROLE_ADMIN)) {
                                        startActivity(new Intent(context, AdminMainActivity.class));
                                    } else {
                                        startActivity(new Intent(context, UserMainActivity.class));
                                    }
                                }
                            } else {
                                // Lấy ra response lỗi
                                String error = Objects.requireNonNull(response.errorBody()).string();

                                // Convert từ String sang JsonObject
                                JsonObject errorResponse = new Gson().fromJson(error, JsonObject.class);

                                // Lấy ra message thông báo lỗi và hiển thị cho người dùng
                                String errorMessage = errorResponse.get("message").getAsString();
                                Log.e(TAG, "Login " + time + " failed: " + errorMessage);
                                txtError.setText(errorMessage);
                                txtError.setVisibility(View.VISIBLE);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Login " + time + " error: " + e.getMessage());
                            Log.getStackTraceString(e);
                            txtError.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onFailure(Call<Account> call, Throwable t) {
                        Log.e(TAG, "Login " + time + " error: " + t.getMessage());
                        Log.getStackTraceString(t);
                        txtError.setVisibility(View.VISIBLE);
                    }
                });
            }
        });

        // Khởi tạo sự kiện bấm Đăng ký
        txtRegister.setOnClickListener(view -> {
            Intent intent = new Intent(context, RegisterActivity.class);
            startActivity(intent);
        });
    }
}