package com.example.qlguixe.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.qlguixe.R;
import com.example.qlguixe.adapter.UserTransportAdapter;
import com.example.qlguixe.api.ApiService;
import com.example.qlguixe.models.Account;
import com.example.qlguixe.models.Statistical;
import com.example.qlguixe.models.Transport;
import com.example.qlguixe.utils.AppConstants;
import com.example.qlguixe.utils.FunctionUtils;
import com.example.qlguixe.utils.SharedPreferenceUtil;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.apache.commons.lang3.StringUtils;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserMainActivity extends AppCompatActivity {
    private final String TAG = "UserMainActivity";
    private final Context context = UserMainActivity.this;

    private TextView txtSurplus;
    private RecyclerView revTransportOfUser;
    private TextView txtNumberTransportInParking;
    private FloatingActionButton btnRegisterTransport;

    private Dialog dialog;
    private List<Transport> transports;
    private UserTransportAdapter userAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Khởi tạo view
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_main);

        // Set background color của action bar là màu đen
        Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(new ColorDrawable(Color.BLACK));

        // Tìm kiếm và ánh xạ các thành phần trong view
        initView();

        // Khởi tạo các listener lắng nghe các hành động
        initListener();
    }

    @SuppressLint("SetTextI18n")
    private void initView() {
        // Tìm kiếm và ánh xạ textView hiển thị tên người dùng
        TextView txtNameUser = findViewById(R.id.txtNameUser);
        txtNameUser.setText(
                FunctionUtils.designTextWhiteAndYellow(
                        "Chủ tài khoản:",
                        SharedPreferenceUtil.getString(AppConstants.USERNAME)));

        // Tìm kiếm và ánh xạ textView hiển thị số dư tài khoản
        txtSurplus = findViewById(R.id.txtSurplus);
        txtSurplus.setText(
                FunctionUtils.designTextWhiteAndYellow(
                        "Số dư tài khoản:",
                        SharedPreferenceUtil.getLong(AppConstants.SURPLUS) + " VNĐ"));

        // Tìm kiếm và ánh xạ textView hiển thị số xe có trong bãi
        txtNumberTransportInParking = findViewById(R.id.txtNumberTransportInParking);

        // Tìm kiếm và ánh xạ recycleView hiển thị danh sách xe của người dùng
        revTransportOfUser = findViewById(R.id.revTransportOfUser);

        // Tìm kiếm và ánh xạ nút đăng ký xe
        btnRegisterTransport = findViewById(R.id.btnRegisterTransport);

        // Lấy ra thời gian xử lý gọi API
        long time = System.currentTimeMillis();

        // Lấy ra và hiển thị số xe đã vào bãi
        Log.i(TAG, "Count number of transports that have entered " + time);
        ApiService.apiService.getAllIn("0").enqueue(new Callback<List<Statistical>>() {
            @Override
            public void onResponse(Call<List<Statistical>> call, Response<List<Statistical>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    Log.i(TAG, "Count number of transports that have entered " + time + " success " + response.body().size());

                    // Hiện số lượng xe đã vào trong bãi
                    txtNumberTransportInParking.setText(
                            FunctionUtils.designTextWhiteAndYellow(
                                    "Số xe đang có trong bãi:",
                                    response.body().size() + "/1000 xe"));
                } else {
                    Log.i(TAG, "Count number of transports that have entered " + time + " failed");

                    // Ẩn dòng thông báo số lượng xe đã vào trong bãi và thông báo lỗi cho người dùng
                    txtNumberTransportInParking.setVisibility(View.GONE);
                    Toast.makeText(context, "Không thể đếm được số xe đang trong bãi", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Statistical>> call, Throwable t) {
                Log.i(TAG, "Count number of transports that have entered " + time + " error: " + t.getMessage());
                Log.getStackTraceString(t);

                // Ẩn dòng thông báo số lượng xe đã vào trong bãi và thông báo lỗi cho người dùng
                txtNumberTransportInParking.setVisibility(View.GONE);
                Toast.makeText(context, "Không thể đếm được số xe đang trong bãi", Toast.LENGTH_SHORT).show();
            }
        });

        // Lấy ra id tài khoản người dùng
        String accountId = SharedPreferenceUtil.getString(AppConstants.ACCOUNT_ID);
        Log.i(TAG, "Get transport of user " + accountId + " time " + time);

        // Lấy ra và hiển thị danh sách xe của người dùng
        ApiService.apiService.getTransportOfUser(accountId).enqueue(new Callback<List<Transport>>() {
            @Override
            public void onResponse(Call<List<Transport>> call, Response<List<Transport>> response) {
                try {
                    if (response.isSuccessful() && response.body() != null) {
                        Log.e(TAG, "Get transport of user " + accountId + " time " + time + " success");

                        transports = response.body();
                        Collections.sort(transports, (transport, t1) -> -(transport.getQr() == null ? "" : transport.getQr()).compareTo(t1.getQr() == null ? "" : t1.getQr()));
                        userAdapter = new UserTransportAdapter(transports, context);

                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
                        revTransportOfUser.setLayoutManager(layoutManager);
                        revTransportOfUser.setAdapter(userAdapter);

                        // TODO: Hiển thị thông tin chi tiết xe
                        userAdapter.setIOnClickItemTransport(
                                (transport, pos) -> Toast.makeText(context, "Bạn vừa bấm vào xe", Toast.LENGTH_SHORT).show());
                    } else {
                        // Lấy ra response lỗi
                        String error = Objects.requireNonNull(response.errorBody()).string();

                        // Convert từ String sang JsonObject
                        JsonObject errorResponse = new Gson().fromJson(error, JsonObject.class);

                        // Lấy ra message thông báo lỗi và hiển thị cho người dùng
                        String errorMessage = errorResponse.get("message").getAsString();
                        Log.e(TAG, "Get transport of user " + accountId + " time " + time + " failed: " + errorMessage);
                        Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Get transport of user " + accountId + " time " + time + " error: " + e.getMessage());
                    Log.getStackTraceString(e);
                    Toast.makeText(context, "Lỗi không xác định !", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<List<Transport>> call, Throwable t) {
                Log.e(TAG, "Get transport of user " + accountId + " time " + time + " error: " + t.getMessage());
                Log.getStackTraceString(t);
                Toast.makeText(context, "Lỗi không xác định !", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initListener() {
        // Khởi tạo sự kiện bấm nút đăng ký xe
        btnRegisterTransport.setOnClickListener(view -> registerTransport());
    }

    private void registerTransport() {
        // Khởi tạo Form đăng ký xe
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.form_register_transport);
        dialog.setCancelable(true);

        // Khởi tạo background đằng sau
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams windowAttributes = window.getAttributes();
        windowAttributes.gravity = Gravity.CENTER_VERTICAL;
        window.setAttributes(windowAttributes);

        // Tìm kiếm và ánh xạ các thành phần trong Form đăng ký xe
        EditText edtTransportType = dialog.findViewById(R.id.edtTransportType);
        EditText edtTransportName = dialog.findViewById(R.id.edtTransportName);
        EditText edtTransportLicense = dialog.findViewById(R.id.edtTransportLicense);
        Button btnRegisterTransport = dialog.findViewById(R.id.btnRegisterTransport);

        // Khởi tạo sự kiện khi bấm nút Đăng ký
        btnRegisterTransport.setOnClickListener(v -> {
            // Lấy ra thời gian xử lý sự kiện
            long time = System.currentTimeMillis();

            // Lấy ra thông tin đăng ký xe
            String transportType = edtTransportType.getText().toString().trim();
            String transportName = edtTransportName.getText().toString().trim();
            String transportLicense = edtTransportLicense.getText().toString().trim();

            // Kiểm tra người dùng đã nhập đầy đủ thông tin trước khi gọi API
            if (StringUtils.isBlank(transportType)
                    || StringUtils.isBlank(transportName)
                    || StringUtils.isBlank(transportLicense)) {
                Toast.makeText(context, "Xin hãy điền đủ thông tin yêu cầu !", Toast.LENGTH_SHORT).show();
            } else {
                // Khởi tạo request đăng ký xe
                Account own = new Account(SharedPreferenceUtil.getString(AppConstants.ACCOUNT_ID));
                Transport request = new Transport(transportType, transportName, transportLicense, own);
                Log.i(TAG, "Register transport request " + time + ": " + request);

                // Gọi API đăng ký xe
                ApiService.apiService.registerTransport(request).enqueue(new Callback<Transport>() {
                    @Override
                    public void onResponse(Call<Transport> call, Response<Transport> response) {
                        try {
                            if (response.isSuccessful()) {
                                Log.i(TAG, "Register transport " + time + " success ");

                                // Thêm xe vào danh sách đang hiển thị và thông báo cho người dùng
                                transports.add(response.body());
                                userAdapter.setList(transports);
                                Toast.makeText(context, "Đăng ký xe thành công !", Toast.LENGTH_SHORT).show();

                                // Ẩn Form đăng ký xe
                                dialog.cancel();
                            } else {
                                // Lấy ra response lỗi
                                String error = Objects.requireNonNull(response.errorBody()).string();

                                // Convert từ String sang JsonObject
                                JsonObject errorResponse = new Gson().fromJson(error, JsonObject.class);

                                // Lấy ra message thông báo lỗi và hiển thị cho người dùng
                                String errorMessage = errorResponse.get("message").getAsString();
                                Log.e(TAG, "Register transport " + time + " failed: " + errorMessage);
                                Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Register transport " + time + " error: " + e.getMessage());
                            Log.getStackTraceString(e);
                            Toast.makeText(context, "Lỗi không xác định !", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Transport> call, Throwable t) {
                        Log.e(TAG, "Register transport " + time + " error: " + t.getMessage());
                        Log.getStackTraceString(t);
                        Toast.makeText(context, "Lỗi không xác định !", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        // Hiển thị Form đăng ký xe
        dialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = new MenuInflater(getBaseContext());
        menuInflater.inflate(R.menu.menu_user, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    @SuppressLint("NonConstantResourceId")
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.itemLogout:
                logout();
                break;
            case R.id.itemChangePassword:
                changePassword();
                break;
            case R.id.itemRecharge:
                recharge();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void logout() {
        // Xóa id tài khoản đang lưu trong hệ thống
        SharedPreferenceUtil.setString(AppConstants.ACCOUNT_ID, null);

        // Xóa username và password nếu người dùng không chọn lưu thông tin
        if (!SharedPreferenceUtil.getBoolean(AppConstants.REMEMBER)) {
            SharedPreferenceUtil.setString(AppConstants.USERNAME, null);
            SharedPreferenceUtil.setString(AppConstants.PASSWORD, null);
        }

        // Xóa số dư tài khoản đang lưu trong hệ thống
        SharedPreferenceUtil.setLong(AppConstants.SURPLUS, 0L);

        // Quay về trang đăng nhập và hiển thị thông báo cho người dùng
        Intent intent = new Intent(context, LoginActivity.class);
        intent.putExtra("notification", "Đã đăng xuất tài khoản !");
        startActivity(intent);
        finish();
    }

    private void changePassword() {
        // Khởi tạo Form đổi mật khẩu
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView( R.layout.form_change_password);
        dialog.setCancelable(true);

        // Khởi tạo background đằng sau
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams windowAttributes = window.getAttributes();
        windowAttributes.gravity = Gravity.CENTER_VERTICAL;
        window.setAttributes(windowAttributes);

        // Tìm kiếm và ánh xạ các thành phần trong Form đổi mật khẩu
        EditText edtOldPassword = dialog.findViewById(R.id.edtOldPassword);
        EditText edtNewPassword = dialog.findViewById(R.id.edtNewPassword);
        EditText edtConfirmPassword = dialog.findViewById(R.id.edtConfirmPassword);
        Button btnChangePassword = dialog.findViewById(R.id.btnChangePassword);

        // Khởi tạo sự kiện khi bấm nút Xác nhận
        btnChangePassword.setOnClickListener(view -> {
            // Lấy ra thông tin đổi mật khẩu
            String oldPassword = edtOldPassword.getText().toString().trim();
            String newPassword = edtNewPassword.getText().toString().trim();
            String confirmPassword = edtConfirmPassword.getText().toString().trim();

            // Kiểm tra request đổi mật khẩu trước khi gọi API
            String error = validateChangePasswordRequest(oldPassword, newPassword, confirmPassword);
            if (StringUtils.isNotBlank(error)) {
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
            } else {
                // Lấy ra thời gian xử lý sự kiện
                long time = System.currentTimeMillis();

                // Khởi tạo request đổi mật khẩu
                Account request = new Account();
                request.set_id(SharedPreferenceUtil.getString(AppConstants.ACCOUNT_ID));
                request.setPassword(newPassword);
                Log.i(TAG, "Change password " + time + " request: " + request);

                ApiService.apiService.updateUser(request.get_id(), request).enqueue(new Callback<Account>() {
                    @Override
                    public void onResponse(Call<Account> call, Response<Account> response) {
                        try {
                            if (response.isSuccessful()) {
                                Log.i(TAG, "Change password " + time + " success ");

                                // Ẩn Form đổi mật khẩu
                                dialog.cancel();

                                // Quay về trang đăng nhập và hiển thị thông báo cho người dùng
                                Intent intent = new Intent(context, LoginActivity.class);
                                intent.putExtra(
                                        "notification",
                                        "Đổi mật khẩu thành công ! Xin hãy đăng nhập lại để tiếp tục sử dụng");
                                startActivity(intent);
                                finish();
                            } else {
                                // Lấy ra response lỗi
                                String error = Objects.requireNonNull(response.errorBody()).string();

                                // Convert từ String sang JsonObject
                                JsonObject errorResponse = new Gson().fromJson(error, JsonObject.class);

                                // Lấy ra message thông báo lỗi và hiển thị cho người dùng
                                String errorMessage = errorResponse.get("message").getAsString();
                                Log.e(TAG, "Change password " + time + " failed: " + errorMessage);
                                Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Change password " + time + " error: " + e.getMessage());
                            Log.getStackTraceString(e);
                            Toast.makeText(context, "Lỗi không xác định !", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Account> call, Throwable t) {
                        Log.e(TAG, "Change password " + time + " error: " + t.getMessage());
                        Log.getStackTraceString(t);
                        Toast.makeText(context, "Lỗi không xác định !", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        // Hiển thị Form đổi mật khẩu
        dialog.show();
    }

    private String validateChangePasswordRequest(
            String oldPassword, String newPassword, String confirmPassword) {
        // Kiểm tra người dùng đã nhập đầy đủ thông tin chưa ?
        if (StringUtils.isBlank(oldPassword)
                || StringUtils.isBlank(newPassword)
                || StringUtils.isBlank(confirmPassword)) {
            return "Xin hãy điền đủ thông tin yêu cầu !";
        }

        // Kiểm tra mật khẩu cũ có đúng hay không ?
        if (!SharedPreferenceUtil.getString(AppConstants.PASSWORD).equals(oldPassword)) {
            return "Mật khẩu cũ không đúng !";
        }

        // Kiểm tra mật khẩu mới có giống mật khẩu cũ ?
        if (oldPassword.equals(newPassword)) {
            return "Mật khẩu mới giống mật khẩu cũ !";
        }

        // Khởi tạo pattern để validate mật khẩu mới có đúng quy định
        // Tối thiểu 6 ký tự trong đó có ít nhất 1 chữ, 1 số và 1 ký tự đặc biệt
        Pattern patternPassword = Pattern.compile("^(?=.*[A-Za-z])(?=.*\\d)(?=.*[@$!%*#?&])[A-Za-z\\d@$!%*#?&]{6,}$");

        // Kiểm tra mật khẩu có đúng với quy định
        if (!patternPassword.matcher(newPassword).find()) {
            return "Mật khẩu mới phải tối thiểu 6 ký tự và có ít nhất 1 chữ, 1 số và 1 ký tự đặc biệt !";
        }

        // Kiểm tra xác nhận mật khẩu có đúng
        if (!newPassword.equals(confirmPassword)) {
            return "Mật khẩu xác nhận không đúng !";
        }

        return "";
    }

    private void recharge() {
        // Khởi tạo Form nạp tiền
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.form_recharge);
        dialog.setCancelable(true);

        // Khởi tạo background đằng sau
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams windowAttributes = window.getAttributes();
        windowAttributes.gravity = Gravity.CENTER_VERTICAL;
        window.setAttributes(windowAttributes);

        // Tìm kiếm và ánh xạ các thành phần trong Form nạp tiền
        EditText edtNumberMoney = dialog.findViewById(R.id.edtNumberMoney);
        Button btnRecharge = dialog.findViewById(R.id.btnRecharge);

        // Khởi tạo sự kiện khi bấm nút Xác nhận
        btnRecharge.setOnClickListener(view -> {
            // Lấy ra số tiền muốn nạp
            long money = Long.parseLong(edtNumberMoney.getText().toString().trim());

            // Validate số tiền muốn nạp trước khi gọi API
            if (money <= 0) {
                Toast.makeText(context, "Số tiền muốn nạp phải lớn hơn 0 !", Toast.LENGTH_SHORT).show();
            } else {
                // Lấy ra thời gian xử lý sự kiện
                long time = System.currentTimeMillis();

                // Khởi tạo request nạp tiền
                Account request = new Account();
                request.set_id(SharedPreferenceUtil.getString(AppConstants.ACCOUNT_ID));
                request.setMoney(SharedPreferenceUtil.getLong(AppConstants.SURPLUS) + money);
                Log.i(TAG, "Recharge " + time + " request: " + request);

                ApiService.apiService.updateUser(request.get_id(), request).enqueue(new Callback<Account>() {
                    @Override
                    public void onResponse(Call<Account> call, Response<Account> response) {
                        try {
                            if (response.isSuccessful()) {
                                Log.i(TAG, "Recharge " + time + " success ");

                                // Thông báo kết quả cho người dùng và hiển thị số dư mới của tài khoản
                                Toast.makeText(context, "Nạp tiền vào tài khoản thành công !", Toast.LENGTH_SHORT).show();
                                SharedPreferenceUtil.setLong(AppConstants.SURPLUS, request.getMoney());
                                txtSurplus.setText(
                                        FunctionUtils.designTextWhiteAndYellow(
                                                "Số dư tài khoản:",
                                                SharedPreferenceUtil.getLong(AppConstants.SURPLUS) + " VNĐ"));

                                // Ẩn Form nạp tiền
                                dialog.cancel();
                            } else {
                                // Lấy ra response lỗi
                                String error = Objects.requireNonNull(response.errorBody()).string();

                                // Convert từ String sang JsonObject
                                JsonObject errorResponse = new Gson().fromJson(error, JsonObject.class);

                                // Lấy ra message thông báo lỗi và hiển thị cho người dùng
                                String errorMessage = errorResponse.get("message").getAsString();
                                Log.e(TAG, "Recharge " + time + " failed: " + errorMessage);
                                Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Recharge " + time + " error: " + e.getMessage());
                            Log.getStackTraceString(e);
                            Toast.makeText(context, "Lỗi không xác định !", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Account> call, Throwable t) {
                        Log.e(TAG, "Recharge " + time + " error: " + t.getMessage());
                        Log.getStackTraceString(t);
                        Toast.makeText(context, "Lỗi không xác định !", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        // Hiển thị Form nạp tiền
        dialog.show();
    }

    @Override
    public void onBackPressed() {
        // Quay về trang đăng nhập và kết thúc
        startActivity(new Intent(context, LoginActivity.class));
        finish();
    }
}