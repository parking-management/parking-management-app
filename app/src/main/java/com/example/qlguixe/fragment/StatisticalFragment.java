package com.example.qlguixe.fragment;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.qlguixe.R;
import com.example.qlguixe.adapter.StatisticalAdapter;
import com.example.qlguixe.api.ApiService;
import com.example.qlguixe.databinding.FragmentStatisticalBinding;
import com.example.qlguixe.models.Statistical;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class StatisticalFragment extends Fragment {

    private final String[] months = {"1", "2", "3"};
    private final String[] days = {"1", "2", "3", "4", "5", "6", "7"};

    private Context context;
    private StatisticalAdapter adapter;
    private List<Statistical> listStatistical;
    private FragmentStatisticalBinding binding;

    private String statisticalDay = "";
    private String statisticalMonth = "";

    public StatisticalFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Khởi tạo biding với Fragment Statistical
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_statistical, container, false);
        context = getContext();

        // Lấy ra và hiển thị danh sách các xe đã rời bãi
        getListTransportHaveLeftParking();

        // Khởi tạo thời gian để chọn thống kê
        generateStatisticalTime();

        // Khởi tạo sự kiện khi chọn thời gian thống kê
        selectTimeParked();

        // Nếu người dùng chọn thống kê theo ngày thì bỏ tích ô chọn thống kê theo tháng
        binding.cbStatisticalByDay.setOnClickListener(view -> {
            if (binding.cbStatisticalByDay.isChecked()) {
                binding.cbStatisticalByMonth.setChecked(false);
            }
        });

        // Nếu người dùng chọn thống kê theo tháng thì bỏ tích ô chọn thống kê theo ngày
        binding.cbStatisticalByMonth.setOnClickListener(view -> {
            if (binding.cbStatisticalByMonth.isChecked()) {
                binding.cbStatisticalByDay.setChecked(false);
            }
        });

        // Khởi tạo sự kiện khi bấm nút Tìm kiếm
        binding.btnSearch.setOnClickListener(view -> {
            if (binding.cbStatisticalByDay.isChecked()) {
                filterByTimeCome(statisticalDay);
            } else if (binding.cbStatisticalByMonth.isChecked()) {
                filterByTimeCome(statisticalMonth);
            } else {
                Toast.makeText(context, "Xin hãy chọn thống kê theo ngày hoặc theo tháng", Toast.LENGTH_SHORT).show();
            }
        });

        // Khởi tạo sự kiện khi người dùng nhập ô tìm kiếm xe theo tên
        binding.edtSearchTransportByName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                String str = editable.toString();
                List<Statistical> list = new ArrayList<>();
                for (Statistical st : listStatistical) {
                    if (st.getTransport().getTrans_name().toLowerCase().contains(str.toLowerCase())) {
                        list.add(st);
                    }
                }
                adapter.setListStatistical(list);
            }
        });

        // Khởi tạo sự kiện khi người dùng bấm nút Reset tìm kiếm
        binding.btnReset.setOnClickListener(view -> {
            // Bỏ chọn checkbox thống kê theo ngày và tháng
            binding.cbStatisticalByDay.setChecked(false);
            binding.cbStatisticalByMonth.setChecked(false);
            // Reset lại ô nhập tìm kiếm xe theo tên
            binding.edtSearchTransportByName.setText("");
            // Lấy ra thống kê danh sách các xe đã gửi trong bài
            getListTransportHaveLeftParking();
        });

        return binding.getRoot();
    }

    private void filterByTimeCome(String timeCome) {
        List<Statistical> list = new ArrayList<>();
        for (Statistical st : listStatistical) {
            if (st.getTimeCome().contains(timeCome)) {
                list.add(st);
            }
        }
        adapter.setListStatistical(list);
    }

    private void selectTimeParked() {
        // Khởi tạo ArrayAdapter để hiển thị danh lựa chọn ngày thống kê
        ArrayAdapter<?> adapterDay = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, days);
        adapterDay.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spnStatisticalByDay.setAdapter(adapterDay);

        // Khởi tạo sự kiện khi người dùng chọn ngày thống kê
        binding.spnStatisticalByDay.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                statisticalDay = days[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        // Khởi tạo ArrayAdapter để hiển thị danh lựa chọn tháng thống kê
        ArrayAdapter<?> adapterMonth = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, months);
        adapterMonth.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spnStatisticalByMonth.setAdapter(adapterMonth);

        // // Khởi tạo sự kiện khi người dùng chọn tháng thống kê
        binding.spnStatisticalByMonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                statisticalMonth = months[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void generateStatisticalTime() {
        // Khởi tạo các ngày để chọn thống kê (Giới hạn được chọn là trong 7 ngày tính từ ngày hôm nay)
        for (int i = 1; i <= 7; i++) {
            days[i - 1] = new SimpleDateFormat("dd-MM-yyyy",
                    Locale.getDefault()).format(new Date(new Date().getTime() - i * (24 * 3600000)));
        }

        // Khởi tạo các tháng để chọn thống kê (Giới hạn được chọn là trong 3 tháng tính từ tháng hôm nay)
        for (int i = 0; i < 3; i++) {
            String[] str = new SimpleDateFormat("dd-MM-yyyy",
                    Locale.getDefault()).format(new Date()).substring(3).split("-");
            int m = Integer.parseInt(str[0]) - i;
            if (m == 0) {
                m = 12;
            } else if (m == -1) {
                m = 11;
            }
            months[i] = m + "-" + str[1];
        }
    }

    private void getListTransportHaveLeftParking() {
        // Lấy ra thời gian xử lý gọi API
        long time = System.currentTimeMillis();

        // Reset lại danh sách xe đã rời bãi
        listStatistical = new ArrayList<>();

        // Gọi API lấy ra danh sách các xe đã ra khỏi bãi
        ApiService.apiService.getAllIn("1").enqueue(new Callback<List<Statistical>>() {
            @Override
            public void onResponse(Call<List<Statistical>> call, Response<List<Statistical>> response) {
                try {
                    if (response.isSuccessful()) {
                        Log.e("StatisticalFragment", "Get list transport have left parking time " + time + " success");

                        // Lấy danh sách các xe đang đã ra khỏi bãi từ response API
                        listStatistical = response.body();

                        // Hiển thị danh sách các xe đã ra khỏi bãi
                        adapter = new StatisticalAdapter(context, listStatistical);
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
                        binding.revStatistical.setLayoutManager(layoutManager);
                        binding.revStatistical.setAdapter(adapter);
                    } else {
                        // Lấy ra response lỗi
                        String error = Objects.requireNonNull(response.errorBody()).string();

                        // Convert từ String sang JsonObject
                        JsonObject errorResponse = new Gson().fromJson(error, JsonObject.class);

                        // Lấy ra message thông báo lỗi và hiển thị cho người dùng
                        String errorMessage = errorResponse.get("message").getAsString();
                        Log.e("StatisticalFragment", "Get list transport have left parking time " + time + " failed: " + errorMessage);
                        Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Log.e("StatisticalFragment", "Get list transport have left parking time " + time + " error: " + e.getMessage());
                    Log.getStackTraceString(e);
                    Toast.makeText(context, "Lỗi không xác định !", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Statistical>> call, Throwable t) {
                Log.e("StatisticalFragment", "Get list transport have left parking time " + time + " error: " + t.getMessage());
                Log.getStackTraceString(t);
                Toast.makeText(context, "Lỗi không xác định !", Toast.LENGTH_SHORT).show();
            }
        });
    }
}