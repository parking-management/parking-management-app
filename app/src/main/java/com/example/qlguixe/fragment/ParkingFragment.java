package com.example.qlguixe.fragment;

import static android.Manifest.permission.CAMERA;
import static com.example.qlguixe.activity.AdminMainActivity.TRANSPORT_GET_IN;
import static com.example.qlguixe.activity.AdminMainActivity.TRANSPORT_GET_OUT;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.qlguixe.R;
import com.example.qlguixe.activity.CaptureActivityImpl;
import com.example.qlguixe.adapter.TransportInParkingAdapter;
import com.example.qlguixe.api.ApiService;
import com.example.qlguixe.databinding.FragmentParkingBinding;
import com.example.qlguixe.models.Statistical;
import com.example.qlguixe.utils.FunctionUtils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.zxing.integration.android.IntentIntegrator;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ParkingFragment extends Fragment {

    @SuppressLint("StaticFieldLeak")
    public static TransportInParkingAdapter transportInParkingAdapter;

    public static List<Statistical> listStatistical;

    @SuppressLint("StaticFieldLeak")
    public static FragmentParkingBinding binding;
    private Context context;

    public ParkingFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Khởi tạo biding với Fragment Parking
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_parking, container, false);
        context = getContext();

        // Lấy ra và hiển thị danh sách các xe đang có trong bãi
        getListTransportInParking();

        // Khởi tạo sự kiện khi bấm nút gửi xe
        binding.btnTransportGetIn.setOnClickListener(view -> {
            if (!checkPermissions()) {
                requestPermission();
            } else {
                TRANSPORT_GET_IN = true;
                transportGetIn();
            }

        });

        // Khởi tạo sự kiện khi bấm nút xuất xe
        binding.btnTransportGetOut.setOnClickListener(view -> {
            TRANSPORT_GET_OUT = true;
            transportGetOut();
        });

        return binding.getRoot();
    }

    private void getListTransportInParking() {
        // Lấy ra thời gian xử lý gọi API
        long time = System.currentTimeMillis();

        // Reset lại danh sách xe đang có trong bãi
        listStatistical = new ArrayList<>();

        // Gọi API lấy ra danh sách các xe đang có trong bãi
        ApiService.apiService.getAllIn("0").enqueue(new Callback<List<Statistical>>() {
            @Override
            public void onResponse(Call<List<Statistical>> call, Response<List<Statistical>> response) {
                try {
                    if (response.isSuccessful()) {
                        Log.e("ParkingFragment", "Get list transport in parking time " + time + " success");

                        // Lấy danh sách các xe đang có trong bãi từ response API
                        listStatistical = response.body();

                        // Hiển thị số xe đang có trong bãi
                        binding.txtNumberTransportInParking.setText(
                                FunctionUtils.designTextWhiteAndYellow(
                                        "Số xe đang có trong bãi:",
                                        listStatistical.size() + "/1000 xe"));

                        // Hiển thị danh sách các xe đang có trong bãi
                        transportInParkingAdapter = new TransportInParkingAdapter(context, listStatistical);
                        RecyclerView.LayoutManager layoutManager =
                                new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
                        binding.revListTransportInParking.setLayoutManager(layoutManager);
                        binding.revListTransportInParking.setAdapter(transportInParkingAdapter);
                    } else {
                        // Lấy ra response lỗi
                        String error = Objects.requireNonNull(response.errorBody()).string();

                        // Convert từ String sang JsonObject
                        JsonObject errorResponse = new Gson().fromJson(error, JsonObject.class);

                        // Lấy ra message thông báo lỗi và hiển thị cho người dùng
                        String errorMessage = errorResponse.get("message").getAsString();
                        Log.e("ParkingFragment", "Get list transport in parking time " + time + " failed: " + errorMessage);
                        Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Log.e("ParkingFragment", "Get list transport in parking time " + time + " error: " + e.getMessage());
                    Log.getStackTraceString(e);
                    Toast.makeText(context, "Lỗi không xác định !", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Statistical>> call, Throwable t) {
                Log.e("ParkingFragment", "Get list transport in parking time " + time + " error: " + t.getMessage());
                Log.getStackTraceString(t);
                Toast.makeText(context, "Lỗi không xác định !", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private boolean checkPermissions() {
        int cameraPermission = ContextCompat.checkSelfPermission(requireContext(), CAMERA);
        return cameraPermission == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(requireActivity(), new String[]{CAMERA}, 200);
    }

    @SuppressLint("QueryPermissionsNeeded")
    private void transportGetIn() {
        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePicture.resolveActivity(requireActivity().getPackageManager()) != null) {
            startActivityForResult(takePicture, 1);
        }
    }

    private void transportGetOut() {
        // Khởi tạo IntentIntegrator dùng để hỗ trợ quét QR Code
        IntentIntegrator intentIntegrator = new IntentIntegrator(getActivity());

        intentIntegrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);

        // Gán giá trị promt text để hướng dẫn người dùng
        intentIntegrator.setPrompt("Bấm nút tăng âm lượng để bật đèn pin");

        // Gán giá trị beep để có tiếng beep khi quét thành công
        intentIntegrator.setBeepEnabled(true);

        // Gán giá trị orientation locked để khóa hướng màn hình khi quét mã QR
        intentIntegrator.setOrientationLocked(true);

        //set capture activity
        intentIntegrator.setCaptureActivity(CaptureActivityImpl.class);

        // Khởi tạo quét
        intentIntegrator.initiateScan();

        // Gọi lại API lấy danh sách các xe đang gửi trong bãi
        getListTransportInParking();
    }
}