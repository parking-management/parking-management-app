package com.example.qlguixe.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.qlguixe.R;
import com.example.qlguixe.adapter.AccountAdapter;
import com.example.qlguixe.api.ApiService;
import com.example.qlguixe.databinding.FragmentAccountBinding;
import com.example.qlguixe.models.Account;
import com.example.qlguixe.utils.FunctionUtils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AccountFragment extends Fragment {
    private final String TAG = "AccountFragment";

    private Dialog dialog;
    private Context context;
    private List<Account> accounts;
    private AccountAdapter accountAdapter;
    private FragmentAccountBinding binding;

    public AccountFragment() {
    }

    public static String validateRegisterRequest(
            String username, String password, String confirmPassword, String name, String phoneNumber,
            String address, String dob, String email) {
        // Khởi tạo các pattern để validate
        Pattern patternPhone = Pattern.compile("^[0-9]*$");
        Pattern patternEmail = Pattern.compile("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$");
        Pattern patternPassword = Pattern.compile("^(?=.*[A-Za-z])(?=.*\\d)(?=.*[@$!%*#?&])[A-Za-z\\d@$!%*#?&]{6,}$");

        // Kiểm tra người dùng đã nhập đầy đủ thông tin chưa ?
        if (StringUtils.isBlank(username)
                || StringUtils.isBlank(password)
                || StringUtils.isBlank(confirmPassword)
                || StringUtils.isBlank(phoneNumber)
                || StringUtils.isBlank(address)
                || StringUtils.isBlank(dob)
                || StringUtils.isBlank(email)) {
            return "Xin hãy điền đủ thông tin yêu cầu !";
        }

        // Kiểm tra độ dài tên đăng nhập
        if (username.length() < 3) {
            return "Tên đăng nhập phải tối thiểu trên 3 kí tự !";
        }

        // Kiểm tra mật khẩu có đúng với quy định
        // Tối thiểu 6 ký tự trong đó có ít nhất 1 chữ, 1 số và 1 ký tự đặc biệt
        if (!patternPassword.matcher(password).find()) {
            return "Mật khẩu phải tối thiểu 6 ký tự và có ít nhất 1 chữ, 1 số và 1 ký tự đặc biệt !";
        }

        // Kiểm tra mật khẩu nhập lại
        if (!password.equals(confirmPassword)) {
            return "Mật khẩu nhập lại không đúng !";
        }

        // Kiểm tra tên chủ tài khoản có hợp lệ ?
        if (StringUtils.isBlank(name)) {
            return "Tên chủ tài khoản không hợp lệ !";
        }

        // Kiểm tra số điện thoại có hợp lệ ?
        if (phoneNumber.length() < 10 || !patternPhone.matcher(phoneNumber).find()) {
            return "Số điện thoại không hợp lệ !";
        }

        // Kiểm tra email có hợp lệ ?
        if (!patternEmail.matcher(email).find()) {
            return "Email không hợp lệ !";
        }

        return "";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Khởi tạo biding với Fragment Account
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_account, container, false);
        context = getContext();

        // Lấy ra danh sách các tài khoản có trong hệ thống
        getAllAccount();

        // Khởi tạo sự kiện khi bấm nút thêm tài khoản
        binding.btnAddAccount.setOnClickListener(view -> addAccount());

        return binding.getRoot();
    }

    public void getAllAccount() {
        // Lấy ra thời gian xử lý gọi API
        long time = System.currentTimeMillis();

        // Reset lại danh sách tài khoản trong hệ thống
        accounts = new ArrayList<>();

        // Gọi API lấy ra danh sách tài khoản trong hệ thống
        ApiService.apiService.getAllAccount().enqueue(new Callback<List<Account>>() {
            @Override
            public void onResponse(Call<List<Account>> call, Response<List<Account>> response) {
                try {
                    if (response.isSuccessful()) {
                        Log.e(TAG, "Get list account in system " + time + " success");

                        // Lấy ra danh sách tài khoản trong hệ thống
                        accounts = response.body();

                        // Hiển thị số tài khoản có trong hệ thống
                        binding.txtNumberAccountInSystem.setText(
                                FunctionUtils.designTextWhiteAndYellow(
                                        "Số tài khoản đang có:",
                                        accounts.size() + " tài khoản"));

                        // Hiển thị danh sách tài khoản trong hệ thống
                        accountAdapter = new AccountAdapter(accounts, getContext());
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
                        binding.revAccountInSystem.setLayoutManager(layoutManager);
                        binding.revAccountInSystem.setAdapter(accountAdapter);

                        // Khởi tạo sự kiện khi bấm chọn một tài khoản bất kì
                        accountAdapter.setListener(account -> showDetailAccountDialog(account));
                    } else {
                        // Lấy ra response lỗi
                        String error = Objects.requireNonNull(response.errorBody()).string();

                        // Convert từ String sang JsonObject
                        JsonObject errorResponse = new Gson().fromJson(error, JsonObject.class);

                        // Lấy ra message thông báo lỗi và hiển thị cho người dùng
                        String errorMessage = errorResponse.get("message").getAsString();
                        Log.e(TAG, "Get list account in system " + time + " failed: " + errorMessage);
                        Toast.makeText(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Get list account in system " + time + " error: " + e.getMessage());
                    Log.getStackTraceString(e);
                    Toast.makeText(getContext(), "Lỗi không xác định !", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Account>> call, Throwable t) {
                Log.e(TAG, "Get list account in system " + time + " error: " + t.getMessage());
                Log.getStackTraceString(t);
                Toast.makeText(getContext(), "Lỗi không xác định !", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void addAccount() {
        // Khởi tạo Form thêm tài khoản
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.form_add_account);
        dialog.setCancelable(true);

        // Khởi tạo background đằng sau
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams windowAttributes = window.getAttributes();
        windowAttributes.gravity = Gravity.CENTER_VERTICAL;
        window.setAttributes(windowAttributes);

        // Tìm kiếm và ánh xạ nút Đăng ký
        Button btnRegister = dialog.findViewById(R.id.btnRegister);

        // Tìm kiếm, ánh xạ và tạm ẩn textView thông báo lỗi
        TextView txtError = dialog.findViewById(R.id.txtError);
        txtError.setVisibility(View.GONE);

        // Tìm kiếm và ánh xạ các editText
        EditText edtUsername = dialog.findViewById(R.id.edtUsername);
        EditText edtPassword = dialog.findViewById(R.id.edtPassword);
        EditText edtConfirmPassword = dialog.findViewById(R.id.edtConfirmPassword);
        EditText edtName = dialog.findViewById(R.id.edtName);
        EditText edtPhoneNumber = dialog.findViewById(R.id.edtPhoneNumber);
        EditText edtAddress = dialog.findViewById(R.id.edtAddress);
        EditText edtDateOfBirth = dialog.findViewById(R.id.edtDateOfBirth);
        EditText edtEmail = dialog.findViewById(R.id.edtEmail);

        // Khởi tạo sự kiện khi bấm Đăng ký
        btnRegister.setOnClickListener(view -> {
            // Lấy ra thông tin đăng ký tài khoản
            String username = edtUsername.getText().toString().trim();
            String password = edtPassword.getText().toString().trim();
            String confirmPassword = edtConfirmPassword.getText().toString().trim();
            String name = edtName.getText().toString().trim();
            String phoneNumber = edtPhoneNumber.getText().toString().trim();
            String address = edtAddress.getText().toString().trim();
            String dob = edtDateOfBirth.getText().toString().trim();
            String email = edtEmail.getText().toString().trim();

            // Kiểm tra request đăng ký tài khoản trước khi gọi API
            String error = validateRegisterRequest(
                    username, password, confirmPassword, name, phoneNumber, address, dob, email);
            if (StringUtils.isNotBlank(error)) {
                txtError.setText(error);
                txtError.setVisibility(View.VISIBLE);
            } else {
                // Lấy ra thời gian xử lý sự kiện
                long time = System.currentTimeMillis();

                // Khởi tạo request đăng ký tài khoản
                Account request = new Account(username, password, name, phoneNumber, address, dob, email);
                Log.i(TAG, "Add account " + time + " request: " + request);

                // Gọi API đăng ký tài khoản
                ApiService.apiService.register(request).enqueue(new Callback<Account>() {
                    @Override
                    public void onResponse(Call<Account> call, Response<Account> response) {
                        try {
                            if (response.isSuccessful()) {
                                Log.i(TAG, "Add account " + time + " success ");

                                // Ẩn thông báo lỗi nếu có
                                txtError.setVisibility(View.GONE);

                                // Hiển thị tài khoản mới thêm vào danh sách tài khoản trong hệ thống
                                accounts.add(response.body());
                                accountAdapter.setAccounts(accounts);

                                // Hiển thị lại số tài khoản có trong hệ thống
                                binding.txtNumberAccountInSystem.setText(
                                        FunctionUtils.designTextWhiteAndYellow(
                                                "Số tài khoản đang có:",
                                                accounts.size() + " tài khoản"));

                                // Thông báo kết quả và đóng Form
                                Toast.makeText(context, "Thêm tài khoản thành công !", Toast.LENGTH_SHORT).show();
                                dialog.cancel();
                            } else {
                                // Lấy ra response lỗi
                                String error = Objects.requireNonNull(response.errorBody()).string();

                                // Convert từ String sang JsonObject
                                JsonObject errorResponse = new Gson().fromJson(error, JsonObject.class);

                                // Lấy ra message thông báo lỗi và hiển thị cho người dùng
                                String errorMessage = errorResponse.get("message").getAsString();
                                Log.e(TAG, "Add account " + time + " failed: " + errorMessage);
                                txtError.setText(errorMessage);
                                txtError.setVisibility(View.VISIBLE);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Add account " + time + " error: " + e.getMessage());
                            Log.getStackTraceString(e);
                            txtError.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onFailure(Call<Account> call, Throwable t) {
                        Log.e(TAG, "Add account " + time + " error: " + t.getMessage());
                        Log.getStackTraceString(t);
                        txtError.setVisibility(View.VISIBLE);
                    }
                });
            }
        });

        // Hiển thị Form thêm tài khoản
        dialog.show();
    }

    public void showDetailAccountDialog(Account account) {
        // Khởi tạo Dialog hiển thị thông tin chi tiết tài khoản
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_detail_account);
        dialog.setCancelable(true);

        // Khởi tạo background đằng sau
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams windowAttributes = window.getAttributes();
        windowAttributes.gravity = Gravity.CENTER_VERTICAL;
        window.setAttributes(windowAttributes);

        // Tìm kiếm và ánh xạ các thành phần trên Dialog
        TextView txtAccountName = dialog.findViewById(R.id.txtAccountName);
        TextView txtAccountPhone = dialog.findViewById(R.id.txtAccountPhone);
        TextView txtAccountEmail = dialog.findViewById(R.id.txtAccountEmail);
        TextView txtAccountAddress = dialog.findViewById(R.id.txtAccountAddress);
        TextView txtAccountDob = dialog.findViewById(R.id.txtAccountDob);
        TextView txtAccountMoney = dialog.findViewById(R.id.txtAccountMoney);
        Button btnRechargeAccount = dialog.findViewById(R.id.btnRechargeAccount);
        Button btnDeleteAccount = dialog.findViewById(R.id.btnDeleteAccount);

        txtAccountName.setText(
                FunctionUtils.designTextBlackAndBlue(
                        "Chủ tài khoản:",
                        account.getName()));
        txtAccountPhone.setText(
                FunctionUtils.designTextBlackAndBlue(
                        "Số điện thoại:",
                        account.getPhone()));
        txtAccountEmail.setText(
                FunctionUtils.designTextBlackAndBlue(
                        "Email:",
                        account.getEmail()));
        txtAccountAddress.setText(
                FunctionUtils.designTextBlackAndBlue(
                        "Địa chỉ:",
                        account.getAddress()));
        txtAccountDob.setText(
                FunctionUtils.designTextBlackAndBlue(
                        "Ngày sinh:",
                        account.getDob()));
        txtAccountMoney.setText(
                FunctionUtils.designTextBlackAndBlue(
                        "Số dư tài khoản:",
                        account.getMoney() + " VNĐ"));

        // Khởi tạo sự kiện khi bấm nút Nạp tiền
        btnRechargeAccount.setOnClickListener(view -> {
            dialog.cancel();
            rechargeForAccount(account);
        });

        // Khởi tạo sự kiện khi bấm nút Xóa tài khoản
        btnDeleteAccount.setOnClickListener(view -> {
            deleteAccount(account);
            dialog.cancel();
        });


        // Hiển thị Dialog thông tin chi tiết tài khoản
        dialog.show();
    }

    public void rechargeForAccount(Account account) {
        // Khởi tạo Dialog nạp tiền cho tài khoản
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.form_recharge);
        dialog.setCancelable(true);

        // Khởi tạo background đằng sau
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams windowAttributes = window.getAttributes();
        windowAttributes.gravity = Gravity.CENTER_VERTICAL;
        window.setAttributes(windowAttributes);

        // Tìm kiếm và ánh xạ các thành phần trong Form nạp tiền cho tài khoản
        TextView titleFormRecharge = dialog.findViewById(R.id.titleFormRecharge);
        EditText edtNumberMoney = dialog.findViewById(R.id.edtNumberMoney);
        Button btnRecharge = dialog.findViewById(R.id.btnRecharge);

        // Set lại tiêu đề Form nạp tiền cho tài khoản
        titleFormRecharge.setText("Nạp tiền cho tài khoản");

        // Khởi tạo sự kiện khi bấm nút Xác nhận
        btnRecharge.setOnClickListener(view -> {
            // Lấy ra số tiền muốn nạp
            long money = Long.parseLong(edtNumberMoney.getText().toString().trim());

            // Validate số tiền muốn nạp trước khi gọi API
            if (money <= 0) {
                Toast.makeText(context, "Số tiền muốn nạp phải lớn hơn 0 !", Toast.LENGTH_SHORT).show();
            } else {
                // Lấy ra thời gian xử lý sự kiện
                long time = System.currentTimeMillis();

                // Khởi tạo request nạp tiền
                Account request = new Account();
                request.set_id(account.get_id());
                request.setMoney(account.getMoney() + money);
                Log.i(TAG, "Recharge " + time + " request: " + request);

                ApiService.apiService.updateUser(request.get_id(), request).enqueue(new Callback<Account>() {
                    @Override
                    public void onResponse(Call<Account> call, Response<Account> response) {
                        try {
                            if (response.isSuccessful()) {
                                Log.i(TAG, "Recharge " + time + " success ");

                                // Thông báo kết quả cho người dùng và set lại thông tin số dư mới của tài khoản
                                Toast.makeText(context, "Nạp tiền vào tài khoản thành công !", Toast.LENGTH_SHORT).show();
                                account.setMoney(request.getMoney());
                                accounts.set(accounts.indexOf(account), account);
                                accountAdapter.setAccounts(accounts);

                                // Ẩn Form nạp tiền cho tài khoản
                                dialog.cancel();
                            } else {
                                // Lấy ra response lỗi
                                String error = Objects.requireNonNull(response.errorBody()).string();

                                // Convert từ String sang JsonObject
                                JsonObject errorResponse = new Gson().fromJson(error, JsonObject.class);

                                // Lấy ra message thông báo lỗi và hiển thị cho người dùng
                                String errorMessage = errorResponse.get("message").getAsString();
                                Log.e(TAG, "Recharge " + time + " failed: " + errorMessage);
                                Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Recharge " + time + " error: " + e.getMessage());
                            Log.getStackTraceString(e);
                            Toast.makeText(context, "Lỗi không xác định !", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Account> call, Throwable t) {
                        Log.e(TAG, "Recharge " + time + " error: " + t.getMessage());
                        Log.getStackTraceString(t);
                        Toast.makeText(context, "Lỗi không xác định !", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        // Hiển thị Form nạp tiền cho tài khoản
        dialog.show();
    }

    public void deleteAccount(Account account) {
        // Lấy ra thời gian xử lý
        long time = System.currentTimeMillis();

        new AlertDialog.Builder(getContext()).setTitle("Thông báo")
                .setMessage(
                        "Bạn có chắc chắn muốn xóa tài khoản {} không ?"
                                .replace("{}", account.getUsername()))
                .setPositiveButton("xóa",
                        (dialogInterface, i) -> ApiService.apiService
                                .deleteAccount(account.get_id())
                                .enqueue(new Callback<Account>() {
                                    @Override
                                    public void onResponse(Call<Account> call, Response<Account> response) {
                                        try {
                                            if (response.isSuccessful()) {
                                                Log.i(TAG, "Delete account " + account.getUsername() + " time " + time + " success ");

                                                // Hiển thị lại danh sách tài khoản trong hệ thống
                                                accounts.remove(account);
                                                accountAdapter.setAccounts(accounts);

                                                // Hiển thị lại số tài khoản có trong hệ thống
                                                binding.txtNumberAccountInSystem.setText(
                                                        FunctionUtils.designTextWhiteAndYellow(
                                                                "Số tài khoản đang có:",
                                                                accounts.size() + " tài khoản"));

                                                // Hiển thị thông báo cho người dùng
                                                Toast.makeText(context, "Xóa tài khoản thành công !", Toast.LENGTH_SHORT).show();
                                            } else {
                                                // Lấy ra response lỗi
                                                String error = Objects.requireNonNull(response.errorBody()).string();

                                                // Convert từ String sang JsonObject
                                                JsonObject errorResponse = new Gson().fromJson(error, JsonObject.class);

                                                // Lấy ra message thông báo lỗi và hiển thị cho người dùng
                                                String errorMessage = errorResponse.get("message").getAsString();
                                                Log.e(TAG, "Delete account " + account.getUsername() + " time " + time + " failed: " + errorMessage);
                                                Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
                                            }
                                        } catch (Exception e) {
                                            Log.e(TAG, "Delete account " + account.getUsername() + " time " + time + " error: " + e.getMessage());
                                            Log.getStackTraceString(e);
                                            Toast.makeText(context, "Lỗi không xác định !", Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<Account> call, Throwable t) {
                                        Log.e(TAG, "Delete account " + account.getUsername() + " time " + time + " error: " + t.getMessage());
                                        Log.getStackTraceString(t);
                                        Toast.makeText(context, "Lỗi không xác định !", Toast.LENGTH_SHORT).show();
                                    }
                                }))
                .setNegativeButton("Hủy", (dialogInterface, i) -> {
                })
                .show();
    }
}