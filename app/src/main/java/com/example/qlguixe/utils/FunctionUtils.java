package com.example.qlguixe.utils;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.Html;
import android.text.Spanned;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;

import com.example.qlguixe.R;

import org.apache.commons.lang3.StringUtils;

import java.util.regex.Pattern;

public class FunctionUtils {

    public static Spanned designText(String colorStr1, String str1, String colorStr2, String str2) {
        String strHtml =
                "<span style=\"color: " + colorStr1 + ";\"> "
                        + str1 +
                        " </span> <span style=\"color: " + colorStr2 + ";\"> "
                        + str2 +
                        " </span>";
        return Html.fromHtml(strHtml);
    }

    @SuppressLint("ResourceType")
    public static Spanned designTextWhiteAndYellow(String str1, String str2) {
        return FunctionUtils.designText("white", str1, "#FFEB3B", str2);
    }

    @SuppressLint("ResourceType")
    public static Spanned designTextBlackAndBlue(String str1, String str2) {
        return FunctionUtils.designText("black", str1, "#1865E8", str2);
    }

    @SuppressLint("ResourceType")
    public static Spanned designTextBlackAndRed(String str1, String str2) {
        return FunctionUtils.designText("black", str1, "#ED1C1C", str2);
    }
}
