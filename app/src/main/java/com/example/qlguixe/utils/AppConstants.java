package com.example.qlguixe.utils;

public class AppConstants {
    public static final String ACCOUNT_ID = "account_id";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String SURPLUS = "surplus";
    public static final String REMEMBER = "remember";

    public static final String ROLE_USER = "user";
    public static final String ROLE_ADMIN = "admin";
}
