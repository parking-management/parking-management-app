package com.example.qlguixe.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.qlguixe.R;
import com.example.qlguixe.click.IOnClickItemTransport;
import com.example.qlguixe.models.Transport;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import org.apache.commons.lang3.StringUtils;

import java.util.List;

public class UserTransportAdapter extends RecyclerView.Adapter<UserTransportAdapter.ViewHolder> {

    private final Context context;

    private List<Transport> list;
    private IOnClickItemTransport iOnClickItemTransport;

    public UserTransportAdapter(List<Transport> list, Context context) {
        this.list = list;
        this.context = context;
    }

    public void setIOnClickItemTransport(IOnClickItemTransport iOnClickItemTransport) {
        this.iOnClickItemTransport = iOnClickItemTransport;
    }

    public List<Transport> getList() {
        return list;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setList(List<Transport> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item_user_transport, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        Transport transport = list.get(position);

        holder.txtTypeTransport.setText(transport.getTrans_type());
        holder.txtNameTransport.setText(transport.getTrans_name());
        holder.txtLicenseTransport.setText(transport.getTrans_license());

        // Nếu có trả về QR để lấy xe ra thì tạo mã QR để quét
        if (StringUtils.isNotBlank(transport.getQr())) {
            try {
                // Khởi tạo MultiFormatWriter
                MultiFormatWriter writer = new MultiFormatWriter();

                // Khởi tạo BitMatrix
                BitMatrix matrix = writer.encode(transport.getQr(), BarcodeFormat.QR_CODE, 500, 500);

                // Khởi tạo BarcodeEncoder
                BarcodeEncoder encoder = new BarcodeEncoder();

                // Khởi tạo Bitmap
                Bitmap bitmap = encoder.createBitmap(matrix);

                // Hiển thị ảnh QR cho người dùng
                holder.imgQr.setImageBitmap(bitmap);
            } catch (WriterException e) {
                Log.i("TransportAdapter", "Lỗi tạo mã QR !");
                Log.getStackTraceString(e);
            }
        }

        holder.llItemTransport.setOnClickListener(view -> iOnClickItemTransport.iOnClick(transport, position));
    }

    @Override
    public int getItemCount() {
        return list != null ? list.size() : 0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgQr;
        LinearLayout llItemTransport;
        TextView txtTypeTransport, txtNameTransport, txtLicenseTransport;

        public ViewHolder(View itemView) {
            super(itemView);
            txtTypeTransport = itemView.findViewById(R.id.txtTypeTransport);
            txtNameTransport = itemView.findViewById(R.id.txtNameTransport);
            txtLicenseTransport = itemView.findViewById(R.id.txtLicenseTransport);

            imgQr = itemView.findViewById(R.id.imgQr);

            llItemTransport = itemView.findViewById(R.id.llItemTransport);
        }
    }
}
