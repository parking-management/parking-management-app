package com.example.qlguixe.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.qlguixe.R;
import com.example.qlguixe.models.Account;
import com.example.qlguixe.utils.FunctionUtils;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class AccountAdapter extends RecyclerView.Adapter<AccountAdapter.ViewHolder> {

    private final Context context;

    private List<Account> accounts;
    private IOnItemAccountClickListener listener;

    public AccountAdapter(List<Account> list, Context context) {
        this.accounts = list;
        this.context = context;
    }

    public void setListener(IOnItemAccountClickListener listener) {
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.item_account, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AccountAdapter.ViewHolder holder, int position) {
        Account account = accounts.get(position);
        holder.txtAccountName.setText(
                FunctionUtils.designTextBlackAndBlue(
                        "Chủ tài khoản:",
                        account.getName()));
        holder.txtAccountPhoneNumber.setText(
                FunctionUtils.designTextBlackAndBlue(
                        "Số điện thoại:",
                        account.getPhone()));
        holder.txtAccountEmail.setText(
                FunctionUtils.designTextBlackAndBlue(
                        "Email:",
                        account.getEmail()));
        holder.txtAccountAddress.setText(
                FunctionUtils.designTextBlackAndBlue(
                        "Địa chỉ:",
                        account.getAddress()));

        holder.llItemAccountInSystem.setOnClickListener(view -> listener.onItemClick(account));
    }

    @Override
    public int getItemCount() {
        return accounts.size();
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setAccounts(List<Account> list) {
        accounts = list;
        notifyDataSetChanged();
    }

    public interface IOnItemAccountClickListener {
        void onItemClick(Account account);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout llItemAccountInSystem;
        TextView txtAccountName, txtAccountPhoneNumber, txtAccountEmail, txtAccountAddress;

        public ViewHolder(View itemView) {
            super(itemView);

            txtAccountName = itemView.findViewById(R.id.txtAccountName);
            txtAccountPhoneNumber = itemView.findViewById(R.id.txtAccountPhoneNumber);
            txtAccountEmail = itemView.findViewById(R.id.txtAccountEmail);
            txtAccountAddress = itemView.findViewById(R.id.txtAccountAddress);
            llItemAccountInSystem = itemView.findViewById(R.id.llItemAccountInSystem);
        }
    }
}
