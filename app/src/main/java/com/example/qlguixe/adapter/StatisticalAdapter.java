package com.example.qlguixe.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.qlguixe.R;
import com.example.qlguixe.models.Statistical;
import com.example.qlguixe.utils.FunctionUtils;

import java.util.List;

public class StatisticalAdapter extends RecyclerView.Adapter<StatisticalAdapter.ViewHolder> {

    private final Context context;

    private List<Statistical> listStatistical;

    public StatisticalAdapter(Context context, List<Statistical> list) {
        this.context = context;
        this.listStatistical = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item_transport_in_parking, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(StatisticalAdapter.ViewHolder holder, int position) {
        Statistical statistical = listStatistical.get(position);
        holder.txtTypeTransport.setText(
                FunctionUtils.designTextBlackAndBlue(
                        "Loại xe:",
                        statistical.getTransport().getTrans_type()));
        holder.txtNameTransport.setText(
                FunctionUtils.designTextBlackAndBlue(
                        "Tên xe:",
                        statistical.getTransport().getTrans_name()));
        holder.txtOwnTransport.setText(
                FunctionUtils.designTextBlackAndBlue(
                        "Chủ xe:",
                        statistical.getTransport().getOwn().getUsername()));
        holder.txtLicenseTransport.setText(
                FunctionUtils.designTextBlackAndBlue(
                        "Biển số:",
                        statistical.getTransport().getTrans_license()));
        holder.txtTimeGetIn.setText(
                FunctionUtils.designTextBlackAndBlue(
                        "Thời gian vào:",
                        statistical.getTimeCome()));
        holder.txtTimeGetOut.setText(
                FunctionUtils.designTextBlackAndBlue(
                        "Thời gian ra:",
                        statistical.getTimeOut()));
        holder.txtTimeGetOut.setVisibility(View.VISIBLE);
    }

    @Override
    public int getItemCount() {
        return listStatistical != null ? listStatistical.size() : 0;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setListStatistical(List<Statistical> statisticals) {
        listStatistical = statisticals;
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout llItemTransportInParking;
        TextView txtTypeTransport, txtNameTransport, txtOwnTransport, txtLicenseTransport, txtTimeGetIn, txtTimeGetOut;

        public ViewHolder(View itemView) {
            super(itemView);
            txtTypeTransport = itemView.findViewById(R.id.txtTypeTransport);
            txtNameTransport = itemView.findViewById(R.id.txtNameTransport);
            txtOwnTransport = itemView.findViewById(R.id.txtOwnTransport);
            txtLicenseTransport = itemView.findViewById(R.id.txtLicenseTransport);
            txtTimeGetIn = itemView.findViewById(R.id.txtTimeGetIn);
            txtTimeGetOut = itemView.findViewById(R.id.txtTimeGetOut);
            llItemTransportInParking = itemView.findViewById(R.id.llItemTransportInParking);
        }
    }
}