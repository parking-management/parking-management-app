package com.example.qlguixe.click;

import com.example.qlguixe.models.Statistical;

public interface IOnClickItemStatistical {
    void iOnClickItemStatistical(Statistical statistical, int position);
}
