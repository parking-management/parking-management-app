package com.example.qlguixe.click;

import com.example.qlguixe.models.Account;

public interface IOnItemAccountClickListener {
    void onItemClick(Account account, int position);
}
